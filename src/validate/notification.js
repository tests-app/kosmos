const NotificationFormat = {
    type: 'object',
    required: ['template'],
    properties: {
        template: { 
            type: 'string'
        }
    }
}

module.exports = {
    NotificationFormat
}
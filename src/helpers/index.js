const fs = require('fs');
const path = require('path');

const requireInFolder = function (Path) {
    const obj = {};
    const items = fs.readdirSync(Path)
    items.forEach(item => {
        item = item.split('.')[0];
        if (item !== 'index') {
            obj[item] = require(path.join(Path, item))
        }
    })
    return obj
};

const requireFolder = function (Path) {
    let obj = {}
    const folders = fs.readdirSync(Path)
    folders.forEach(folder => {
        folder = folder.split('.')[0];
        obj[folder] = requireInFolder(path.join(Path, folder))
    })
    return obj
}

module.exports = {
    requireInFolder,
    requireFolder,
};
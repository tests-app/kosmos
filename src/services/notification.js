const _ = require('lodash')

const {
    Players,
    Notifications,
    Sequelize
} = db;

const {
    Op
} = Sequelize
const errorCode = require('../modules/error')
const vkApi = require('../modules/vk-api')
const Log = require('../modules/log')
const vkDataLimit = parseInt(process.env.VK_DATA_LIMIT);
const Rabbit = require('../modules/rabbitMq')
module.exports = {
    
    async NotifyPlayers(template, offset = 0, notifyId = null, regenerateOptions = null) {
        let notificationId;
        let players;

        try {
            if (!notifyId) {
                const notification = await Notifications.create({
                    template
                })
                notificationId = notification.id
            } else {
                notificationId = notifyId
            }

            if (!~template.indexOf('{user_name}')) {
                players = await Players.getNoTNotifiedLimitPlayers(vkDataLimit, offset)
                if (players.length > 0) {

                    players[0].get({
                        plain: true
                    })

                    Rabbit.sendRabbit('notification', Buffer.from(JSON.stringify({
                        notificationId,
                        template,
                        players,
                        regenerateOptions: {
                            offset
                        }
                    })))

                    await this.NotifyPlayers(template, offset + vkDataLimit, notificationId)

                } else {
                    await Notifications.Done(notificationId)
                }

            } else {
                let listPlayers = await Players.getListName()

                if (regenerateOptions && regenerateOptions.first_name) {
                    const index = _.findIndex(listPlayers, function (o) {
                        return o.first_name === regenerateOptions.first_name;
                    });
                    listPlayers = _.slice(listPlayers, index)
                }

                for (let player of listPlayers) {

                    player = player.get({
                        plain: true
                    })

                    const playerCount = parseInt(player.count)

                    const text = template.replace(/{user_name}/g, player.first_name)

                    if (playerCount <= vkDataLimit) {

                        players = await Players.getNoTNotifiedLimitPlayers(vkDataLimit, 0, player.first_name)

                        Rabbit.sendRabbit('notification', Buffer.from(JSON.stringify({
                            notificationId,
                            template: text,
                            players,
                            regenerateOptions: {
                                first_name: player.first_name
                            }
                        })))

                    } else {
                        const cyrcle = Math.ceil(playerCount / vkDataLimit)
                        let start = (regenerateOptions != null && regenerateOptions.hasOwnProperty('start')) ? regenerateOptions.start : 0
                        for (start; start < cyrcle; start++) {

                            if(regenerateOptions && start === regenerateOptions.start && player.first_name === regenerateOptions.first_name) continue

                            const offset = start * vkDataLimit

                            players = await Players.getNoTNotifiedLimitPlayers(vkDataLimit, offset, player.first_name)

                            Rabbit.sendRabbit('notification', Buffer.from(JSON.stringify({
                                notificationId,
                                template: text,
                                players,
                                regenerateOptions: {
                                    first_name: player.first_name,
                                    start
                                }
                            })))
                        }
                    }
                }

                await Notifications.Done(notificationId)
            }
        } catch (error) {
            console.log(error)
        }
    },
    async sendMessages(players, text) {
        const ids = players.map(player => player.vk_id)
        const arrNotifyPlayers = await vkApi.sendNotification(ids, text)
        await Log.append(`Оповещено ${arrNotifyPlayers.length} / ${ids.length}`)
        return arrNotifyPlayers
    },
    async RegenerateNotify() {
        console.log('RegenerateNotify Start')
        let NotDoneNotification = await Notifications.findAll({
            where: {
                done: false
            }
        })
        if (NotDoneNotification.length > 0) {
            await Log.append('RegenerateNotify')
            for (let notify of NotDoneNotification) {
                notify = notify.get({
                    plain: true
                })
                const offset = notify.regenerate_options['offset'] +  vkDataLimit || 0
                await this.NotifyPlayers(notify.template, offset, notify.id, notify.regenerate_options)
            }
        }
    }
}
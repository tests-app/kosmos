const NotificationService = require('../../services/notification')

module.exports = async function (req, res, next) {
  try {
    const template = req.body.template || req.query.template;
    if (!template) throw new Error('template not found')
    NotificationService.NotifyPlayers(template)
    res.status(200).json({
      ok: true
    })
  } catch (error) {
    res.status(500).json({
      ok: false,
      code: error.code,
      message: error.message
    })
  }
};
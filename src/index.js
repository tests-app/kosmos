const express = require('express');
const path = require('path');
const router = express.Router();

const {
  Validator,
  ValidationError,
} = require('express-json-validator-middleware');

const validator = new Validator({
  allErrors: true,
});

const validatorValidate = validator.validate;

const {
  requireFolder
} = require('./helpers');

const ctrl = requireFolder(path.join(__dirname, 'controllers'))
const {
  NotificationFormat
} = require('./validate/notification')

// Notification
router
  .get('/send', validatorValidate({
    query: NotificationFormat
  }), ctrl.notification.send)
  .post('/send', validatorValidate({
    body: NotificationFormat
  }), ctrl.notification.send)

router.use(async (err, req, res, next) => {
  if (err instanceof ValidationError) {
    res.status(400).json({
      ok: false,
      err,
      message: 'Неверный формат входных данных',
    });
    next();
  } else next(err);
});

module.exports = router;
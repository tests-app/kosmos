module.exports = {
    1: {
        code: 1,
        message: 'Too frequently'
    },
    2: {
        code: 2,
        message: 'Server fatal error'
    },
    3: {
        code: 3,
        message: 'Invalid data'
    },
}
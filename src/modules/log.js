const fs = require('fs');
const errorCode = require('./error');
const logFile = 'log.txt';
module.exports = {
    append(text) {
        // console.log(text)
        text += '\n\r';
        return new Promise((resolve, reject) => {
            fs.appendFile(logFile, text, function (err) {
                if (err) {
                    error.code = errorCode[2].code
                    error.message = errorCode[2].message
                    reject(error)
                }
                resolve()
            })
        })
    },
    async resNotificationLog(all,notyfied) {
        const str = `
        --------------------------- \n\r
        Оповещение окончено \n\r
        Всего:  ${all} \n\r
        Оповещено:  ${notyfied} \n\r
        --------------------------- \n\r
        `
        await this.append(str)
    },
}
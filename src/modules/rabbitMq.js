require('dotenv').config();
const amqp = require('amqplib/callback_api');
const connect = new Promise((resolve, reject) => {
    amqp.connect(process.env.RABBIT_CONNECT, function (error0, connection) {
        if (error0) reject(error0)

        const queue = 'notification';

        connection.createChannel(function (error1, channel) {
            if (error1) reject(error1)

            channel.assertQueue(queue, {
                durable: true
            });
            resolve({
                connection,
                channel
            })
        });
    });
})

const sendRabbit = async (queue, msg) => {
    const conn = await connect
    conn.channel.sendToQueue(queue, Buffer.from(msg), {
        persistent: true
    });
}

module.exports = {
    connect,
    sendRabbit
}
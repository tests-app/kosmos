const moment = require('moment');

const errorCode = require('./error');

const Log = require('../modules/log');

const {
    VkBase
} = db

module.exports = {
    async sendNotification(ids, text) {
        const error = new Error()
        let log = '';
        let resId = [];
        if (!Array.isArray(ids) || ids.length > 100 || !text) {
            error.code = errorCode[3].code
            error.message = errorCode[3].message
            throw error
        }
        const promise = new Promise(async (resolve, reject) => {
            try {
                if (!this.checkFrequently()) {
                    error.code = errorCode[1].code
                    error.message = errorCode[1].message
                    throw error
                } else {
                    for (const id of ids) {
                        const itemVk = await VkBase.findOne({
                            attributes: ['notify'],
                            where: {
                                vk_id: id
                            }
                        })
                        if (itemVk && itemVk.notify === true) {
                            resId.push(id)
                            log += `vk_id = ${id} | Сообщение доставлено Дата: ${new Date().toString()} | Сообщение: ${text} \n\r`
                        }
                    }
                    await Log.append(log);
                    resolve(resId)
                }
            } catch (error) {
                reject(error)
            }
        })
        return promise
    },
    checkFrequently() {
        let result = true;
        if (!global.hasOwnProperty('vkApi')) {
            global.vkApi = {
                start: moment(),
                count: 0
            }
        }

        global.vkApi.count += 1
        const secondsDiff = global.vkApi.start.diff(moment())

        if (global.vkApi.count > 3 && secondsDiff > 1) {
            result = false
        }

        if (global.vkApi.count > 3) {
            global.vkApi.start = moment()
            global.vkApi.count = 0
        }

        return result
    }
};
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Players', {
      vk_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false,
      },
      first_name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    })
    let arr = []
    let vkId = 1;
    for (let i = 1; i <= 1000; i++) {
      for (let j = 1; j <= 135; j++) {
        arr.push({
          vk_id: vkId,
          first_name: 'Player'+i,
          createdAt: new Date(),
          updatedAt: new Date()
        })
        vkId+=1
      }
    }
    for (let index = 135001; index <= 200000; index++) {
      arr.push({
        vk_id: index,
        first_name: 'Players' + index,
        createdAt: new Date(),
        updatedAt: new Date()
      })
    }
    await queryInterface.bulkInsert('Players', arr)
  },
  down: async queryInterface => {
    await queryInterface.bulkDelete('Players')
    await queryInterface.dropTable('Players')
  },
};
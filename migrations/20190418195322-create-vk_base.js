module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('VkBase', {
      vk_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false,
      },
      first_name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      notify: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    })
    let arr = []
    for (let index = 1; index <= 200000; index++) {
      let obj = {
        vk_id: index,
        first_name: 'Player' + index,
        createdAt: new Date(),
        updatedAt: new Date()
      }
      if(index % 10 === 0) {
        obj.notify = false
      } else {
        obj.notify = true
      }
      arr.push(obj)
    }
    await queryInterface.bulkInsert('VkBase', arr)
  },
  down: async queryInterface => {
    await queryInterface.bulkDelete('VkBase')
    await queryInterface.dropTable('VkBase')
  },
};
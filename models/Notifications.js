module.exports = (sequelize, DataTypes) => {
  const Notifications = sequelize.define('Notifications', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    template: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    done: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    regenerate_options: {
      type: DataTypes.JSON,
      defaultValue: null
    }
  }, {
    tableName: 'Notifications',
  });

  Notifications.Done = async (id) => {
    const notification = await Notifications.findOne({
      where: {
        id
      }
    })
    notification.done = true
    notification.save()
    return notification
  }

  Notifications.UpdateRegenerateOptions = async (id, params) => {
    const notification = await Notifications.findOne({
      where: {
        id
      }
    })
    if(notification) {
      notification.regenerate_options = params
      notification.save();
    }
  }
  return Notifications;
};
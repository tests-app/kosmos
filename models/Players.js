module.exports = (sequelize, DataTypes) => {
  const Players = sequelize.define('Players', {
    vk_id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    tableName: 'Players',
  });

  Players.getListName = async () => {
    return await Players.findAll({
      attributes: ['first_name', [sequelize.fn('COUNT', sequelize.col('first_name')), 'count']],
      group: ['first_name'],
      order: ['first_name'],
      having: sequelize.literal('count(first_name) > 1')
    })
  }

  Players.getNoTNotifiedLimitPlayers = async (limit, offset = 0, name = false) => {
    const whereObj = {}

    if(name) {
      whereObj.first_name = name
    }
    return await Players.findAll({
      where: whereObj,
      limit,
      offset
  })
  }

  return Players;
};
var amqp = require('amqplib/callback_api');
require('dotenv').config();
require('./models');
const {
    Notifications,
} = db;
const NotificationServise = require('./src/services/notification')
const errorCode = require('./src/modules/error')
amqp.connect(process.env.RABBIT_CONNECT, function (error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function (error1, channel) {
        if (error1) {
            throw error1;
        }

        var queue = 'notification';

        channel.assertQueue(queue, {
            durable: true
        });

        channel.prefetch(1)

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

        channel.consume(queue, async function (msg) {
            try {
                const data = JSON.parse(msg.content)
                await NotificationServise.sendMessages(data.players, data.template)
                await Notifications.UpdateRegenerateOptions(data.notificationId, data.regenerateOptions)
            } catch (error) {
                console.log(error.message)
                if (error.code === errorCode[1].code) {
                    await Log.append(`Вызов VKApi превысил лимит ${error.message} code:${error.code}`)
                    channel.sendToQueue(queue, Buffer.from(data), {
                        persistent: true
                    });
                } else {
                    await Log.append(`Завершено с ошибкой ${error.message} code:${error.code}`)
                }
            }
            channel.ack(msg);
        }, {
            noAck: false
        });
    });
});
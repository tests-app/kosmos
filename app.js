const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
require('dotenv').config();
require('./models');

const app = express();

const {PORT} = process.env;

app.use(cors({
  origin: '*',
  optionsSuccessStatus: 200
}))

app.use(express.json());

app.use(express.urlencoded({
  extended: false,
}));

app.use(cookieParser());

const routes = require('./src');

app.use('/', routes);

app.listen(PORT, async (error) => {
  if (error) console.log(error);
});

process.on('uncaughtException', (err) => {
  console.log('Caught exception: ', err);
});

module.exports = app;
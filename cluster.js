var cluster = require('cluster');
var numCPUs = require('os').cpus().length;
require('dotenv').config();
require('./models');
const NotificationService = require('./src/services/notification');
if (cluster.isMaster) {
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }
    cluster.on('fork', (worker) => {
        console.log('worker ' + worker.process.pid + ' fork');
    });
    cluster.on('listening', (worker, address) => {
        console.log(`A worker is now connected to ${address.address}:${address.port}`);
    });
    cluster.on('exit', (worker, code, signal) => {
        console.log('worker ' + worker.process.pid + ' exit');
    });

    NotificationService.RegenerateNotify()
} else {
    require("./app.js");
}